/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/23 19:45:37 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/24 15:15:21 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>
#include <stdlib.h>

int *ft_range(int min, int max)
{
    int *tab;
    int i;
    
    i = 0;
    if(min >= max)
    {
        return(0);
    }
    
    //tab = NULL;
    tab = malloc(sizeof(int) * (max - min));
    while(min < max)
    {
        //printf("%d\n", min);
        //printf("%d\n", tab[i]);
        tab[i] = min;
        printf("%d\n", tab[i]);
        min++;
        i++;
        //printf("%d\n", tab[i]);
    }
    //printf("%d\n", tab[i]);
    return(tab);
}

int main()
 {
     int min = 0;
     int max = 5;
     int *tab;
     int i = 0;
     tab = ft_range(min,max);
     while(tab[i])
     {
         printf("%d\n", tab[i]);
     }
 }


