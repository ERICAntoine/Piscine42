/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 17:22:53 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/24 18:37:41 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include <stdio.h>

int ft_ultimate_range(int **range, int min, int max)
{
    int *tab;
    int i;
    
    i = 0;
    if(min >= max)
    {
        return(0);
    }
    
    //tab = NULL;
    tab = malloc(sizeof(int) * (max - min));
    while(min < max)
    {
        //printf("%d\n", min);
        //printf("%d\n", tab[i]);
        tab[i] = min;
        //printf("%d\n", tab[i]);
        min++;
        i++;
        //printf("%d\n", tab[i]);
    }
    //printf("%d\n", tab[i]);
    *range = tab;
    return(range);
}

int main()
{
    int min = -25;
    int max = 25;
    int range = 5;
    int *tab;
    int i = 0;
    tab = ft_ultimate_range(range,min,max);
    while(tab[i])
    {
        printf("%d", tab[i]);
        i++;
    }
}

