/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/24 18:46:37 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/24 18:48:12 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int    ft_strlen(char *str)
{
    int i;
    
    i = 0;
    while (str[i] != '\0')
    {
        i++;
    }
    return (i);
}

char    *ft_strcat(char *dest, char *src)
{
    int d;
    int s;
    
    d = 0;
    s = 0;
    while (dest[d] != '\0')
    {
        d++;
    }
    while (src[s] != '\0')
    {
        dest[d] = src[s];
        d++;
        s++;
    }
    dest[d] = '\0';
    return (dest);
}

char *ft_concat_params(int argc, char **argv)
{
    int i;
    int length;
    char *str;
    
    i = 1;
    length = argc;
    while(i < argc)
    {
        length += ft_strlen(argv[i++]);
    }
    i = 1;
    //str[0] = 0;
    str = malloc(sizeof(char) * (length + 1));
    if((str = malloc(sizeof(char) * (length + 1))) == NULL)
    {
        return (0);
    }
    while(i < argc)
    {
        str = ft_strcat(str, argv[i++]);
        str = ft_strcat(str, "\n");
        //printf("%s", str);
    }
    //printf("%s", str);
    //str[i] = '\0';
    return(str);
}


int main(int argc, char **argv)
{
    char    *str;
    
        str = ft_concat_params(argc, argv);
        printf("String result:\n%s", str);
    return (0);
}
