/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <eantoine@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/07 10:01:48 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/07 20:26:53 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_comb2(void)
{
	int nbr1;
	int nbr2;

	nbr1 = 0;
	nbr2 = 0;
	while (nbr1 <= 98)
	{
		nbr2 = nbr1 + 1;
		while (nbr2 <= 99)
		{
			ft_putchar((nbr1 / 10) + 48);
			ft_putchar((nbr1 % 10) + 48);
			ft_putchar(' ');
			ft_putchar((nbr2 / 10) + 48);
			ft_putchar((nbr2 % 10) + 48);
			if (nbr1 != 98 || nbr2 != 99)
			{
				ft_putchar(',');
				ft_putchar(' ');
			}
			nbr2++;
		}
		nbr1++;
	}
}
