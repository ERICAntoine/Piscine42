/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <eantoine@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 16:30:34 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/07 19:40:48 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print(char one, char two, char three)
{
	ft_putchar(one);
	ft_putchar(two);
	ft_putchar(three);
	if (!(one == '7' && two == '8' && three == '9'))
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_comb(void)
{
	char zero;
	char one;
	char two;

	one = '1';
	two = '2';
	zero = '0';
	while (zero <= '7')
	{
		one = zero + 1;
		while (one <= '8')
		{
			two = one + 1;
			while (two <= '9')
			{
				ft_print(zero, one, two);
				two++;
			}
			one++;
		}
		zero++;
	}
}

int main(){
	ft_print_comb();
	return(0);
}
