/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 16:37:16 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/19 11:16:28 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strupcase(char *str)
{
	int	i;

	i = 0;
	if (str[i] >= 97 && str[i] <= 122)
	{
		str[i] = str[i] - 32;
		i++;
	}
	return (str);
}

char	*ft_strlowcase(char *str)
{
	int i;

	i = 0;
	if (str[i] >= 65 && str[i] <= 90)
	{
		str[i] = str[i] + 32;
	}
	return (str);
}

char	*ft_strcapitalize(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 65 && str[i] <= 90)
		{
			str[i] = str[i] + 32;
		}
		while ((str[i] >= ' ' && str[i] <= '/') ||
		(str[i] >= ':' && str[i] <= '@') ||
		(str[i] == '\v') || (str[i] == '\n') ||
		(str[i] == '\r') || (str[i] == '\f') ||
		(str[i] == '\t') || (str[i] >= '[' && str[i] <= '`') ||
		(str[i] >= '{' && str[i] <= '~'))
		{
			i++;
			if (str[i] >= 'a' && str[i] <= 'z')
				str[i] = str[i] - 32;
		}
		if (str[0] >= 'a' && str[0] <= 'z')
			str[0] = str[0] - 32;
		i++;
	}
	return (str);
}
