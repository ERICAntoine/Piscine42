/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 15:13:29 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/15 11:12:38 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	char *ps1;
	char *ps2;

	if (to_find == 0)
	{
		return (str);
	}
	while (*str != 0)
	{
		ps1 = str;
		ps2 = to_find;
		while (*ps1 == *ps2 && *ps1 != 0 && *ps2 != 0)
		{
			ps1++;
			ps2++;
		}
		if (*ps2 == 0)
		{
			return (str);
		}
		str++;
	}
	return (0);
}
