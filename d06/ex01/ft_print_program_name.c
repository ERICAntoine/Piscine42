/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_program_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/19 18:21:23 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/23 14:24:26 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int put;

	put = 0;
	while (str[put] > '\0')
	{
		ft_putchar(str[put]);
		put++;
	}
	ft_putchar('\n');
}

int		main(int argc, char **argv)
{
	if (argc)
	{
		ft_putstr(argv[0]);
	}
}
