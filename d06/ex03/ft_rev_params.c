/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/20 16:37:23 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/21 14:42:56 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putstr(char *str)
{
	int put;

	put = 0;
	while (str[put] > '\0')
	{
		ft_putchar(str[put]);
		put++;
	}
	ft_putchar('\n');
}

int		main(int argc, char **argv)
{
	argc -= 1;
	while (argc > 0)
	{
		ft_putstr(argv[argc]);
		argc--;
	}
}
