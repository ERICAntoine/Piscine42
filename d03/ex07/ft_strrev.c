/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <eantoine@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/10 11:16:09 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/10 12:59:22 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrev(char *str)
{
	int		i;
	int		a;
	char	temp;

	i = 0;
	a = 0;
	while (str[a] != '\0')
	{
		a++;
	}
	a = a - 1;
	while (i < a)
	{
		temp = str[a];
		str[a] = str[i];
		str[i] = temp;
		a--;
		i++;
	}
	return (str);
}
