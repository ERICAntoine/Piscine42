/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/11 17:04:05 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/14 10:44:49 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdio.h>

int	ft_is_prime(int nb)
{
	int mod;

	mod = 2;
	while ((nb % mod) != 0)
	{
		if (mod > nb)
		{
			return (0);
		}
		mod++;
	}
	if (mod == nb)
	{
		return (1);
	}
	return (0);
}

int main()
{
	printf("%d", ft_is_prime(6));
	return(0);
}
