/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eantoine <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 11:10:52 by eantoine          #+#    #+#             */
/*   Updated: 2018/07/14 10:49:45 by eantoine         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int	ft_is_prime(int nb)
{
	int mod;

	mod = 2;
	while ((nb % mod) != 0)
	{
		if (mod > nb)
		{
			return (0);
		}
		mod++;
		if (mod == nb)
		{
			return (1);
		}
	}
	return (0);
}

int	ft_find_next_prime(int nb)
{
	while (ft_is_prime(nb) == 1)
	{
		return (nb);
	}
	while (ft_is_prime(nb) == 0)
	{
		nb++;
	}
	return (nb);
}

int main()
{
	printf("%d", ft_find_next_prime(6));
	return(0);
}
